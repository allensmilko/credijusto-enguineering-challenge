# Generated by Django 3.2.3 on 2021-05-24 05:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProviderRate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('provider', models.CharField(max_length=200)),
                ('last_updated', models.CharField(max_length=200)),
                ('value', models.FloatField()),
            ],
        ),
    ]
