from django.db import models

class ProviderRate(models.Model):
    provider = models.CharField(max_length=200)
    last_updated = models.CharField(max_length=200)
    value = models.FloatField()