from django.urls import path
from .views import GetRates

urlpatterns = [
    path('rates/', GetRates.as_view()),
]