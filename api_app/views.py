from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from .models import ProviderRate
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from .utils import scraper, api_consumer

@method_decorator(csrf_exempt, name='dispatch')
class GetRates(View):
    def get(self, request):
        rates = list()

        api_response = api_consumer.Consume()
        rates.extend([api_response])
        
        scrapedData = scraper.Scrape()
        rates.extend(scrapedData)

        data = {
            "rates": rates
        }

        return JsonResponse(data, status=200)