import requests

url = "http://data.fixer.io/api/latest?access_key=7072c231daf72792eb8c2c7f35fb97da&symbols=MXN"

def Consume():
    payload={}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    successResponse = response.json()
    return {
        "provider": "provider_2",
        "last_updated": successResponse["date"].replace("-", "/"),
        "value": successResponse["rates"]["MXN"]
    }