import requests
# import urllib.request
# import time
from bs4 import BeautifulSoup

url = "https://www.banxico.org.mx/tipcamb/tipCamMIAction.do"

def Scrape():
    response = requests.get(url)
    data = []
    soup = BeautifulSoup(response.text, "html.parser")
    row = soup.find_all("tr", class_="renglonNon")
    for  i in range(len(row)):
        currentRow = row[i].findChildren("td" , recursive=False)
        dataForDump = {}
        currentDate = currentRow[0].text.replace("\r", "").replace("\n", "").replace(" ", "")
        dataForDump['provider'] = "provider_1"
        dataForDump['last_updated'] = currentDate
        dataForDump['value'] = float(currentRow[3].text.replace("\r", "").replace("\n", "").replace(" ", ""))
        data.extend([dataForDump])
    return data
            