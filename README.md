# Credijusto Challenge

This sis a python3 and django api made for get exchange history from diferents sources

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip3 install -r ./requeriments.txt 
```

## Run the server

```python
python manage.py runserver
```
after run this command  you can check the healthcheck on the next url

```
GET
http://127.0.0.1:8000/rates
```

the response is like the next json format

```
{
    "rates": [
        {
            "provider": "provider_2",
            "last_updated": "2021/05/24",
            "value": 24.395702
        },
        {
            "provider": "provider_1",
            "last_updated": "24/05/2021",
            "value": 19.881
        },
        {
            "provider": "provider_1",
            "last_updated": "22/05/2021",
            "value": 19.881
        },
        {
            "provider": "provider_1",
            "last_updated": "20/05/2021",
            "value": 19.8313
        },
        {
            "provider": "provider_1",
            "last_updated": "18/05/2021",
            "value": 19.8487
        }
    ]
}
```